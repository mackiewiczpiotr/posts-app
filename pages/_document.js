import React from "react";

import Document, { Head, Main, NextScript } from "next/document";

export default class MyDocument extends Document {

    render() {
        const { buildManifest } = this.props;
        const { css } = buildManifest;
        return (
            <html lang="fa" dir="rtl">
            <Head>
                {css.map(file => (
                    <link rel="stylesheet" href={`/_next/${file}`} key={file} />
                ))}
            </Head>
            <body>
            <Main />
            <NextScript />
            </body>
            </html>
        );
    }
}