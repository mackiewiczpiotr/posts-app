import React, {Component} from 'react';
import {Button} from "antd";
import Link from "next/link";
import PostForm from "../components/postForm";

export default class AddPost extends Component {

    constructor(props) {
        super(props);

        this.state = {
            postSubmitting: false,
            postSubmitted: false,
            postError: false,
            submittedPostId: ''
        };

        this.postAdded = this.postAdded.bind(this);
    }

    postAdding() {
        this.setState({ postSubmitting: true });
    }

    postAdded(postId) {
        this.setState({ postSubmitting: false, postSubmitted: true, submittedPostId: postId })
    }

    render() {
        if(this.state.postSubmitted) {
            return (
                <div className="container">
                    <h3>Add Post</h3>
                    <p>Your post has been submitted</p>
                    <p>You can see your post or go back to posts list</p>
                    <Link href="/">
                        <Button type="primary" icon="home" size="large">Home</Button>
                    </Link>
                    <Link href={{ pathname: '/post-details', query: { id: this.state.submittedPostId } }}>
                        <Button type="primary" size="large">See your post</Button>
                    </Link>
                </div>
            )
        }

        if(this.state.postSubmitting) {
            return (
                <div className="container">
                    <h3>Add Post</h3>
                    <p>Submitting post...</p>
                </div>
            )
        }

        return (
            <div className="container">
                <h3>Add Post</h3>
                <PostForm onSaving={this.postAdding.bind(this)} onSave={this.postAdded}/>
                <hr/>
                <Link href="/">
                    <Button type="primary" icon="home" size="large">Home</Button>
                </Link>
            </div>
        )

    }
}