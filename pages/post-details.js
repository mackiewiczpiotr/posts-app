import React, {Component} from 'react';
import Link from 'next/link';
import { Button, Card } from 'antd';
import CommentForm from "../components/commentForm";

import { List } from 'antd';

import { API_URL } from '../env-config';

import moment from 'moment';

export default class PostDetails extends Component {

    constructor(props) {
        super(props);

        this.state = {
            post: {}
        };

        this.commentAdded = this.commentAdded.bind(this);
    }

    componentDidMount() {
        fetch(`${API_URL}/posts/${this.props.url.query.id}`, {
            method: 'GET'
        })
            .then(response => response.json())
            .then(data => {
                this.setState({ post: data.post });
            })
            .catch((err) => {
                console.log(err);
            });
    }

    renderComments() {
        return this.state.post.comments.map(post => {
            return (
                <li key={post.id}>
                    <p>{post.text}</p>
                    <p>{post.author_nickname}</p>
                </li>
            )
        })
    }

    commentAdded(newComment) {
        let post = this.state.post;
        post.comments.push(newComment);
        this.setState({ post: post })
    }

    render() {
        return (
            <div className="container">
                <div style={{ background: '#ECECEC', padding: '30px' }}>
                    <Card title={this.state.post.text} bordered={false} >
                        { moment(this.state.post.created_at).format('MMMM Do YYYY, h:mm:ss a') }
                    </Card>
                </div>
                <hr/>
                <h3>Comments</h3>
                <List
                    itemLayout="horizontal"
                    dataSource={this.state.post.comments}
                    renderItem={item => (
                        <List.Item>
                            <List.Item.Meta
                                title={item.author_nickname}
                                description={item.text}
                            />
                        </List.Item>
                    )}
                />
                <hr/>
                <CommentForm postId={this.state.post.id} onSave={this.commentAdded}/>
                <hr/>
                <Link href="/">
                    <Button type="primary" icon="home" size="large">Home</Button>
                </Link>
            </div>
        )
    }
}
