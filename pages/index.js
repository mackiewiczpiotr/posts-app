import React, {Component} from 'react';
import Link from 'next/link';
import fetch from 'isomorphic-unfetch';

import {Button, List} from 'antd';

import "../style.css"

import { API_URL } from '../env-config';

export default class Index extends Component {

    static async getInitialProps() {
        const res = await fetch(`${API_URL}/posts`);
        const data = await res.json();
        return { posts: data.posts };
    }

    renderPostList() {
        return (
            <List
                itemLayout="horizontal"
                dataSource={this.props.posts}
                renderItem={item => (
                    <div>
                        <Link href={{ pathname: '/post-details', query: { id: item.id } }}>
                            <a>
                                <List.Item>
                                    <List.Item.Meta
                                        title={item.text}
                                    />
                                </List.Item>
                            </a>
                        </Link>
                        <hr/>
                    </div>
                )}>

            </List>
        )
    }

    render() {
        return (
            <div className="container">
                {this.renderPostList()}
                <Link href="/add-post">
                    <Button type="primary" icon="plus" size="large">New Post</Button>
                </Link>
            </div>
        )
    }
}
