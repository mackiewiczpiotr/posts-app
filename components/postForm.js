import React, {Component} from 'react';
import { Button, Form, Input, Row } from 'antd';

import { API_URL } from '../env-config';

const FormItem = Form.Item;
const { TextArea } = Input;

class PostForm extends Component {

    constructor(props) {
        super(props);

        this.state = {
            postSubmitting: false,
            postSubmitted: false,
            postError: false
        }
    }

    handleSubmit = (e) => {
        e.preventDefault();
        this.props.onSaving();
        this.props.form.validateFields((err, values) => {
            if (!err) {
                fetch(`${API_URL}/posts`, {
                    method: 'POST',
                    headers: {
                        'Accept': 'application/json',
                        'Content-Type': 'application/json',
                    },
                    body: JSON.stringify({
                        text: values.text
                    })
                })
                    .then(res => res.json())
                    .then(result => {
                        this.props.onSave(result.post.id)
                    })
            }
        })
    };

    render() {
        const { getFieldDecorator } = this.props.form;
        return (
            <Form onSubmit={this.handleSubmit} className="login-form">
                <Row>
                    <FormItem>
                        {getFieldDecorator('text', {
                            rules: [{ required: true, message: 'Post body is required' }],
                        })(
                            <TextArea type="text" placeholder="Post body" />
                        )}
                    </FormItem>
                </Row>
                <Row>
                    <FormItem>
                        <Button disabled={this.state.postSubmitting} type="primary" htmlType="submit" className="login-form-button">
                            Submit Post
                        </Button>
                    </FormItem>
                </Row>
            </Form>
        )
    }
}

export default Form.create()(PostForm)