import React, {Component} from 'react';
import { Button, Form, Input, Row, Col } from 'antd';

const FormItem = Form.Item;
const { TextArea } = Input;

import { API_URL } from '../env-config';

class CommentForm extends Component {

    constructor(props) {
        super(props);

        this.state = {
            commentSubmitting: false,
            commentSubmitted: false,
            commentError: false,
            author_nickname: '',
            text: ''
        }
    }

    handleSubmit = (e) => {
        e.preventDefault();
        this.props.form.validateFields((err, values) => {
            if (!err) {
                this.setState({ commentSubmitting: true });
                fetch(`${API_URL}/posts/${this.props.postId}/comments`, {
                    method: 'POST',
                    headers: {
                        'Accept': 'application/json',
                        'Content-Type': 'application/json',
                    },
                    body: JSON.stringify({
                        author_nickname: values.author_nickname,
                        text: values.text,
                    })

                })
                    .then(res => res.json())
                    .then(
                        (result) => {
                            this.setState({ commentSubmitted: true, commentSubmitting: false });
                            this.props.form.setFieldsValue({
                                author_nickname: '',
                                text: ''
                            });
                            this.props.onSave(result.comment);
                        },
                        (error) => {
                            this.setState({commentError: error})
                        }
                    )
            }
        })
    };

    render() {
        const { getFieldDecorator } = this.props.form;
        return (
            <div>
                <Form onSubmit={this.handleSubmit} className="login-form">
                    <Row>
                        <Col span={4}>
                            <FormItem>
                                {getFieldDecorator('author_nickname', {
                                    rules: [{ required: true, message: 'Nickname is required' }],
                                })(
                                    <Input type="text" placeholder="Nickname"/>
                                )}
                            </FormItem>
                        </Col>
                    </Row>
                    <Row>
                        <FormItem>
                            {getFieldDecorator('text', {
                                rules: [{ required: true, message: 'Comment body is required' }],
                            })(
                                <TextArea type="text" placeholder="Comment body"/>
                            )}
                        </FormItem>
                    </Row>
                    <Row>
                        <FormItem>
                            <Button disabled={this.state.commentSubmitting} type="primary" htmlType="submit" className="login-form-button">
                                { this.state.commentSubmitting ? '...Submitting comment' : 'Submit Comment' }
                            </Button>
                        </FormItem>
                    </Row>
                </Form>
            </div>
        )
    }
}

export default Form.create()(CommentForm)